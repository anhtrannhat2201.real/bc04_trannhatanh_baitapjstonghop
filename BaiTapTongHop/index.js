var numArr = [];
// kiểm tra số nguyên
let checkPositiveInteger = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/)) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};
//bài 1
document
  .getElementById("scanTable-bai1")
  .addEventListener("click", function () {
    //   console.log("yes");
    document.getElementById("result-bai1").innerHTML = "";
    let count = 1;
    for (let i = 0; i < 10; i++) {
      // tạo chuỗi rống
      let inTr = "";
      // tạo những thẻ tr mới trên giao diện
      let newTr = document.createElement("tr");

      let upperIndex = count + 10;
      for (count; count < upperIndex; count++) {
        //   console.log("count: ", count);
        inTr += `<td>${count}</td>`;
      }
      newTr.innerHTML = inTr;
      document.getElementById("result-bai1").append(newTr);
    }
  });
// bài 2
// click nhập số
document
  .getElementById("btn-themso-bai2")
  .addEventListener("click", function () {
    console.log("yes");

    let inputNum = document.getElementById("inputNum-bai2").value * 1;
    document.getElementById("inputNum-bai2").value = "";
    //   console.log("inputNum: ", inputNum);
    numArr.push(inputNum);
    document.getElementById("result-inputNum-bai2").innerHTML = numArr;
  });
// // hàm kiểm tra số nguyên tố
function kiemTraSoNguyenTo(n) {
  let e = 1;

  if (n < 2) {
    return (e = 0);
  }
  let i = 2;
  while (i < n) {
    if (n % i == 0) {
      e = 0;
      break;
    }
    i++;
  }
  return e;
}

// click tìm ra số nguyên tố
document
  .getElementById("inSoNguyenTo-bai2")
  .addEventListener("click", function () {
    var numPrime = [];

    for (let i = 0; i < numArr.length; i++) {
      let currentNum = numArr[i];
      if (kiemTraSoNguyenTo(currentNum) == 1) {
        numPrime.push(currentNum);
      }
    }
    document.getElementById(
      "result-songuyento-bai2"
    ).innerHTML = `Số nguyên tố: ${numPrime}`;
  });
//   end bài 2
// Bài 3
document.getElementById("btn-bai3").addEventListener("click", function () {
  //   console.log("yes");
  let thamSoN = document.getElementById("inputNum-bai3").value;
  if (thamSoN * 1 > 1) {
    document.getElementById("baoloi").innerText = "";
    thamSoN *= 1;
    let totalNumber = 0;
    for (let i = 2; i <= thamSoN; i++) {
      totalNumber += i;
    }
    totalNumber += 2 * thamSoN;
    document.getElementById(
      "result-bai3"
    ).innerText = `Tổng S = ${totalNumber}`;
  } else {
    document.getElementById(
      "baoloi"
    ).innerText = `N phải lớn hơn 1 và n phải là số nguyên dương`;
    document.getElementById("result-bai3").innerText = "";
  }
});
// end bài 3
// bài 4
document.getElementById("totalResult").addEventListener("click", function () {
  //   console.log("yes");
  let thamSoN = document.getElementById("inputNum-bai4").value * 1;
  if (thamSoN > 0) {
    document.getElementById("baoloi-bai4").innerText = "";
    thamSoN *= 1;
    let arrN = [];
    for (let i = 1; i <= thamSoN; i++) {
      thamSoN % i == 0 && arrN.push(i);
    }
    document.getElementById(
      "result-bai4"
    ).innerHTML = `<p>Tổng số lượng ước của ${thamSoN} là ${arrN.length}.</p>
          <p>Các ước số là: ${arrN.join(", ")}</p>`;
  } else {
    document.getElementById(
      "baoloi-bai4"
    ).innerText = `n phải là số nguyên dương`;
    document.getElementById("result-bai4").innerText = "";
  }
});
// end bài 4
// bài 5
document
  .getElementById("btn-result-bai5")
  .addEventListener("click", function () {
    let inputN = document.getElementById("inputNum-bai5").value;
    if (inputN * 1 > 0) {
      document.getElementById("baoloi-bai5").innerText = "";
      let arrN = inputN.split("");
      arrN.reverse();
      let daonguocN = "";
      arrN.forEach((chuso) => (daonguocN += chuso));
      document.getElementById(
        "result-bai5"
      ).innerText = `Số đảo ngược của ${inputN} là: ${daonguocN}`;
    } else {
      document.getElementById(
        "baoloi-bai5"
      ).innerText = `n phải là số nguyên dương`;
      document.getElementById("result-bai5").innerText = "";
    }
  });
//end bài 5
// bài 6
document
  .getElementById("btn-result-bai6")
  .addEventListener("click", function () {
    console.log("yes");
    let inputX = document.getElementById("inputNum-bai6").value;
    if (inputX * 1 > 0) {
      document.getElementById("baoloi-bai6").innerText = "";
      inputX *= 1;
      let tongX = 0;
      let trueX = null;
      for (let i = 1; tongX <= inputX; i++) {
        tongX += i;
        trueX = i;
      }
      document.getElementById(
        "result-bai6"
      ).innerText = `X nguyên dương lớn nhất là: x = ${trueX - 1}`;
    } else {
      document.getElementById(
        "baoloi-bai6"
      ).innerText = `x phải là số nguyên dương`;
      document.getElementById("result-bai6").innerText = "";
    }
  });
//   end bài 6
// bài 7
document
  .getElementById("btn-result-bai7")
  .addEventListener("click", function () {
    let inputN = document.getElementById("inputNum-bai7").value;
    if (inputN * 1 > 0) {
      document.getElementById("baoloi-bai7").innerText = "";
      inputN *= 1;
      let bangCuuChuong = "";
      for (let i = 1; i < 11; i++) {
        // bảng cửu chương
        bangCuuChuong += `<p class="mb-2">${inputN} x ${i} = ${inputN * i}</p>`;
      }
      //   dom kết quả bảng cửu chương
      document.getElementById("result-bai7").innerHTML = bangCuuChuong;
    } else {
      document.getElementById(
        "baoloi-bai7"
      ).innerText = `n phải là số nguyên dương`;
      document.getElementById("result-bai7").innerText = "";
    }
  });
// end bài 7
// bài 8
//set up bộ bài
let cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
let players = Array.from(Array(4), () => []);
let xulyCards = 0;
document.getElementById("bai8-chiaBai").addEventListener("click", function () {
  if (xulyCards < 12) {
    for (let i = 0; i < 4; i++) {
      players[i].push(cards[0]);
      xulyCards++;
      cards.splice(0, 1);
    }
    document.getElementById("bai8-cards").innerText = cards.join(", ");
    document.getElementById("bai8-player1").innerText = players[0].join(", ");
    document.getElementById("bai8-player2").innerText = players[1].join(", ");
    document.getElementById("bai8-player3").innerText = players[2].join(", ");
    document.getElementById("bai8-player4").innerText = players[3].join(", ");
  } else {
    document.getElementById("baoloi-bai8").innerText = `Đã hết bài`;
  }
});
document.getElementById("bai8-xaoBai").addEventListener("click", function () {
  cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  players = Array.from(Array(4), () => []);
  xulyCards = 0;
  document.getElementById("bai8-cards").innerText = "Đố biết luôn";
  document.getElementById("bai8-player1").innerText = "";
  document.getElementById("bai8-player2").innerText = "";
  document.getElementById("bai8-player3").innerText = "";
  document.getElementById("bai8-player4").innerText = "";
  document.getElementById("baoloi-bai8").innerText = "";
});

// end bài 8
// bài 9
document.getElementById("bai9-timChoGa").addEventListener("click", function () {
  let totalDogChick = document.getElementById("txt-bai9-soCon").value;
  let totalLeg = document.getElementById("txt-bai9-soChan").value;
  let isValid =
    checkPositiveInteger(
      totalDogChick,
      "baoloi-bai9-soCon",
      "Còn cái nịt! Nhập vào số con"
    ) &
    checkPositiveInteger(
      totalLeg,
      "baoloi-bai9-soChan",
      "Phải có chân chứ mọi người ơi! "
    );
  if (isValid) {
    totalDogChick *= 1;
    totalLeg *= 1;
    let chickenArr = [];
    let dogArr = [];
    let totalDog = null;
    let totalChicken = null;
    for (let i = 0; i <= totalDogChick; i++) {
      chickenArr.push(i);
      dogArr.push(i);
    }
    for (let i = 0; i < chickenArr.length; i++) {
      let dogIndex = dogArr.findIndex(
        (dog) => dog * 4 + chickenArr[i] * 2 == totalLeg
      );
      if (dogIndex != -1 && dogArr[dogIndex] + chickenArr[i] == totalDogChick) {
        totalDog = dogArr[dogIndex];
        totalChicken = chickenArr[i];
        break;
      }
    }
    if (totalDog == null) {
      document.getElementById(
        "bai9-result-soCho"
      ).innerText = `Không có Chó, Gà nào thỏa điều kiện số chân và số con`;
      document.getElementById("bai9-result-soGa").innerText = "";
    } else {
      document.getElementById(
        "bai9-result-soCho"
      ).innerText = `Số Con Chó: ${totalDog}`;
      document.getElementById(
        "bai9-result-soGa"
      ).innerText = `Số Con Gà: ${totalChicken}`;
    }
  }
});
// end bài 9
// bài 10
// check giờ
let checkHours = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/) && value * 1 >= 1 && value * 1 <= 12) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};
// check phút
let checkMinutes = (value, errIdAddress, message) => {
  if (value.match(/^\d+$/) && value * 1 >= 0 && value * 1 <= 59) {
    document.getElementById(errIdAddress).innerText = "";
    return true;
  } else {
    document.getElementById(errIdAddress).innerText = message;
    return false;
  }
};
// start onclick bài 10
document.getElementById("bai10-tinhGoc").addEventListener("click", function () {
  let hours = document.getElementById("txt-bai10-hours").value;
  let minutes = document.getElementById("txt-bai10-minutes").value;
  let isValid =
    checkHours(hours, "baoloi-bai10-hours", "Giờ là số nguyên, từ 1 đến 12") &
    checkMinutes(
      minutes,
      "baoloi-bai10-minutes",
      "Phút là số nguyên, từ 0 đến 59"
    );
  if (isValid) {
    hours *= 1;
    minutes *= 1;
    let hoursAngle = (hours / 12) * 360 + (minutes * ((1 / 12) * 360)) / 60;
    let minutesAngle = (minutes / 60) * 360;
    let hoursMinutesAngle = Math.abs(hoursAngle - minutesAngle);
    document.getElementById(
      "bai10-result"
    ).innerHTML = `Góc lệch giữa kim giờ và kim phút là: ${hoursMinutesAngle}<sup>o</sup> hoặc ${
      360 - hoursMinutesAngle
    }<sup>o</sup>`;
  }
});
//  end bài 10
